﻿using Assets.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Game
{
    public class GameCanvasController : MonoBehaviour
    {
        private void Start()
        {
            GameObject.Find("Music").GetComponent<Text>().text = PlayableMusic.Current.Music;
            GameObject.Find("Difficulty").GetComponent<Text>().text = PlayableMusic.Current.Difficulty.ToString();
        }

        private void Update()
        {
        }
    }
}