﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.SelectMenu
{
    public sealed class MusicSelectorController : MonoBehaviour
    {
        private readonly string[] _musics =
        {
            "Get Wild",
            "Get Over",
            "Shooting Star",
            "innocent starter",
            "Hacking to the Gate"
        };

        private Color _activeColor;

        private int _currentIndex;
        private Image _image;
        private Text _musicName;
        private Color _originalColor;

        public string Music
        {
            get { return _musics[_currentIndex]; }
        }

        private void Start()
        {
            _musicName = GameObject.Find("MusicName").GetComponent<Text>();
            _image = GetComponent<Image>();

            var color = _image.color;
            _originalColor = color;
            _activeColor = new Color(color.r, color.g, color.b, 1);

            Select(Offset.Current);
        }

        private void Update()
        {
        }

        public void Activate()
        {
            _image.color = _activeColor;
        }

        public void Deactivate()
        {
            _image.color = _originalColor;
        }

        public void Select(Offset offset)
        {
            _currentIndex = OffsetUtility.Forward(_currentIndex, _musics.Length, offset);
            _musicName.text = _musics[_currentIndex];
        }
    }
}