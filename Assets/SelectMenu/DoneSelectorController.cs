﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.SelectMenu
{
    public class DoneSelectorController : MonoBehaviour
    {
        private Image _image;
        private Color _originalColor;
        private Color _activeColor;
        private Text _text;

        private void Start()
        {
            _image = GetComponent<Image>();
            var color = _image.color;
            _originalColor = color;
            _activeColor = new Color(color.r, color.g, color.b, 1);

            _text = GameObject.Find("Done").GetComponent<Text>();
        }

        private void Update()
        {
        }

        public void Activate()
        {
            _image.color = _activeColor;
            _text.color = Color.white;
        }

        public void Deactivate()
        {
            _image.color = _originalColor;
            _text.color = Color.gray;
        }

        public void Select(Offset offset)
        {
            // 何もしない
        }
    }
}