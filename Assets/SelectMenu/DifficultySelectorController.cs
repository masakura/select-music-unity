﻿using Assets.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.SelectMenu
{
    public sealed class DifficultySelectorController : MonoBehaviour
    {
        private Color _activeColor;
        private Image _image;
        private Color _originalColor;
        private Text[] _texts;
        private int _currentIndex = 1;

        public Difficulty Difficulty
        {
            get
            {
                switch (_currentIndex)
                {
                    case 0: return Difficulty.Easy;
                    case 1: return Difficulty.Normal;
                    case 2: return Difficulty.Hard;
                    default: return Difficulty.Normal;
                }
            }
        }

        private void Start()
        {
            _image = GetComponent<Image>();

            var color = _image.color;
            _originalColor = color;
            _activeColor = new Color(color.r, color.g, color.b, 1);

            _texts = new[]
            {
                GameObject.Find("Easy").GetComponent<Text>(),
                GameObject.Find("Normal").GetComponent<Text>(),
                GameObject.Find("Hard").GetComponent<Text>()
            };

            Select(Offset.Current);
        }

        private void Update()
        {
        }

        public void Activate()
        {
            _image.color = _activeColor;
        }

        public void Deactivate()
        {
            _image.color = _originalColor;
        }

        public void Select(Offset offset)
        {
            _currentIndex = OffsetUtility.Forward(_currentIndex, _texts.Length, offset);

            foreach (var text in _texts)
            {
                text.color = Color.gray;
            }

            _texts[_currentIndex].color = Color.white;
        }
    }
}