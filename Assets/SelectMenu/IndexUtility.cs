﻿namespace Assets.SelectMenu
{
    public static class OffsetUtility
    {
        public static int Forward(int current, int length, Offset offset)
        {
            return (current + (int) offset + length) % length;
        }
    }
}