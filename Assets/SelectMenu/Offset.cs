﻿namespace Assets.SelectMenu
{
    public enum Offset
    {
        Previous = -1,
        Current = 0,
        Next = 1
    }
}