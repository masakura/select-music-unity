﻿using Assets.Common;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.SelectMenu
{
    public sealed class CanvasController : MonoBehaviour
    {
        private int _currentSelectorIndex;
        private DifficultySelectorController _difficultySelector;
        private DoneSelectorController _doneSelector;
        private MusicSelectorController _musicSelector;

        private void Start()
        {
            _musicSelector = GameObject.Find("MusicSelector").GetComponent<MusicSelectorController>();
            _difficultySelector = GameObject.Find("DifficultySelector").GetComponent<DifficultySelectorController>();
            _doneSelector = GameObject.Find("DoneSelector").GetComponent<DoneSelectorController>();

            ForwardSelector(Offset.Current);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.LeftArrow)) ForwardItem(Offset.Previous);
            if (Input.GetKeyDown(KeyCode.RightArrow)) ForwardItem(Offset.Next);

            if (Input.GetKeyDown(KeyCode.UpArrow)) ForwardSelector(Offset.Previous);
            if (Input.GetKeyDown(KeyCode.DownArrow)) ForwardSelector(Offset.Next);

            if (Input.GetKeyDown(KeyCode.Return)) Done();
            if (Input.GetKeyDown(KeyCode.Space)) Done();
        }

        private void ForwardItem(Offset offset)
        {
            switch (_currentSelectorIndex)
            {
                case 0:
                    _musicSelector.Select(offset);
                    break;

                case 1:
                    _difficultySelector.Select(offset);
                    break;

                case 2:
                    _doneSelector.Select(offset);
                    break;
            }
        }

        private void ForwardSelector(Offset offset)
        {
            _currentSelectorIndex = OffsetUtility.Forward(_currentSelectorIndex, 3, offset);

            _musicSelector.Deactivate();
            _difficultySelector.Deactivate();
            _doneSelector.Deactivate();

            switch (_currentSelectorIndex)
            {
                case 0:
                    _musicSelector.Activate();
                    break;

                case 1:
                    _difficultySelector.Activate();
                    break;

                case 2:
                    _doneSelector.Activate();
                    break;
            }
        }

        private void Done()
        {
            if (_currentSelectorIndex == 2)
            {
                PlayableMusic.Current = new PlayableMusic(_musicSelector.Music, _difficultySelector.Difficulty);
                SceneManager.LoadScene("Game");
            }
            else
            {
                ForwardSelector(Offset.Next);
            }
        }
    }
}