﻿namespace Assets.Common
{
    public sealed class PlayableMusic
    {
        public PlayableMusic(string music, Difficulty difficulty)
        {
            Music = music;
            Difficulty = difficulty;
        }

        public string Music { get; private set; }
        public Difficulty Difficulty { get; private set; }

        public static PlayableMusic Current { get; set; }
    }
}